import React, {Component} from 'react'
import classNames from 'classnames'
import {OrderedMap} from 'immutable'
import _ from 'lodash'
import {ObjectID} from '../helpers/objectid'
import SearchUser from './search-user'
import moment from  'moment/moment'
import UserBar from './user-bar'
import momentLocation from  'moment/locale/tr' // tr dil paketi


import LeftBar from './left-bar'


import 'react-chat-elements'  // sonra kaldır çünkü herşey import ediliyor ve performans skıntı
//<EmojiPicker/>

//emoji ikon insert_emoticon
import FaEmojiIcon from 'react-icons/lib/fa/smile-o';
import FaUserIcon from 'react-icons/lib/fa/user';
import FaSendIcon from 'react-icons/lib/fa/paper-plane';
import EmojiPicker from 'emoji-picker-react'; // emojiler yüklendi
import 'emoji-picker-react/dist/universal/style.scss'; // emoji stilleri or any other way you consume scss files

import { Scrollbars } from 'react-custom-scrollbars';

//import ScrollToBottom from 'react-scroll-to-bottom';

///emoji için
//import JSEMOJI from 'emoji-js';



import 'react-chat-elements/dist/main.css';


// MessageBox component
import { MessageBox } from 'react-chat-elements';

import { Popup } from 'react-chat-elements'

import { Button } from 'react-chat-elements'
import { Navbar } from 'react-chat-elements'





//getCurrentUser





export default class Messenger extends Component {

    constructor(props) {


        super(props);

        this.state = {
            height: window.innerHeight,
            newMessage: 'Merhaba. ',
            searchUser: "",
            showSearchUser: false,
            isLogin : props.store.getCurrentUser(),
            isHiddenEmoji : true
        }


/*
          const {store} = this.props
          const currentUser = store.getCurrentUser();

          if (currentUser){

          this.setState({isLogin: true})

          }
*/

        //  this.setState.isLogin=true

        this._onResize = this._onResize.bind(this);
        this.handleSend = this.handleSend.bind(this)
        this.renderMessage = this.renderMessage.bind(this);
        this.scrollMessagesToBottom = this.scrollMessagesToBottom.bind(this)
        this._onCreateChannel = this._onCreateChannel.bind(this);
        this.renderChannelTitle = this.renderChannelTitle.bind(this)
        this.renderChannelAvatars = this.renderChannelAvatars.bind(this);

      //  this.refs.scrollbars.scrollToBottom()

        moment.locale('tr');  // türkçe gün farkı set etme


        const sabitDeneme =                <Navbar
            left={
                <div>'SOL' area</div>
            }
            center={
                <div>'ORTA' area</div>
            }
            right={
                <div>'SAĞ' area</div>
            }/>;


    }

//kullanıcı giriş yapmış mı
        isLogin(){
          const {store} = this.props;
        var user =  store.getCurrentUser();
        if (user) {
          return true;
        }
        else {
          return false;
        }
        }

    renderChannelAvatars(channel){
        const {store} = this.props;

        const members = store.getMembersFromChannel(channel);

        const maxDisplay = 4;
        const total = members.size > maxDisplay ? maxDisplay : members.size;

        const avatars = members.map((user, index) => {



            return index < maxDisplay ?  <img key={index} src={_.get(user, 'avatar')} alt={_.get(user, 'name')} /> : null

        });


        return <div className={classNames('channel-avatars', `channel-avatars-${total}`)}>{avatars}</div>
    }
    renderChannelTitle(channel = null) {

        if (!channel) {
            return null;
        }
        const {store} = this.props;

        const members = store.getMembersFromChannel(channel);


        const names = [];

        members.forEach((user) => {

            const name = _.get(user, 'name');
            names.push(name);
        })

        let title = "Oda : "+_.join(names, ',');

      // let title = "Oda Adı";

        if (!title && _.get(channel, 'isNew')) {
            title = 'Yeni Mesaj';
        }

        return <h2>{title}</h2>
    }

    _onCreateChannel() {

        const {store} = this.props;

        const currentUser = store.getCurrentUser();
        const currentUserId = _.get(currentUser, '_id');

        const channelId = new ObjectID().toString();
        const channel = {
            _id: channelId,
            title: '',
            lastMessage: "",
            members: new OrderedMap(),
            messages: new OrderedMap(),
            isNew: true,
            userId: currentUserId,
            created: new Date(),
        };

        channel.members = channel.members.set(currentUserId, true);


        store.onCreateNewChannel(channel);


    }

    scrollMessagesToBottom() {

        if (this.messagesRef) {

            this.messagesRef.scrollBottom = this.messagesRef.scrollHeight;
        }
        console.log("güncellendi Message to Bottom")
      //  this.Scrollbars.scrollToBottom();
    }



    renderMessage(message) {

        const text = _.get(message, 'body', '');

        const html = _.split(text, '\n').map((m, key) => {

            return <p key={key} dangerouslySetInnerHTML={{__html: m}}/>
        })


        return html;
    }

    handleSend() {

        const {newMessage} = this.state;
        const {store} = this.props;


        // create new message

        if (_.trim(newMessage).length) {

            const messageId = new ObjectID().toString();
            const channel = store.getActiveChannel();
            const channelId = _.get(channel, '_id', null);
            const currentUser = store.getCurrentUser();

            const message = {
                _id: messageId,
                channelId: channelId,
                body: newMessage,
                userId: _.get(currentUser, '_id'),
                me: true,

            };


            store.addMessage(messageId, message);

            this.setState({
                newMessage: '',
            })
        }


    }

    _onResize() {

        this.setState({
            height: window.innerHeight
        });
    }

//mesajlar güncellendiğinde scroll un aşağıya kayması durumu
    componentDidUpdate() {

            //  this.setState({isLogin:this.props.getCurrentUser()})

        this.scrollMessagesToBottom();
        //  this.scrollMessagesToBottom1();

        //login mi ?
        this.isLogin();
    }

    componentDidMount() {


       window.addEventListener('resize', this._onResize);
      //this.refs.scrollbars.scrollToBottom ();

        //this.scrollMessagesToBottom();


    }


    componentWillUnmount() {

        window.removeEventListener('resize', this._onResize)

    }



    EmojiCallback(degisken,degisken1){

    //  var emoji = new EmojiConvertor();
    //  var output1 = replace_unified(degisken);
    //  const {degisken} = this.props;
     alert("emoji tıklandı"+degisken);
      console.log("emoji : "+degisken1);



      //start emoji fetchChannelMessages
/*
      // new instance
jsemoji = new JSEMOJI();
// set the style to emojione (default - apple)
jsemoji.img_set = 'emojione';
// set the storage location for all emojis
jsemoji.img_sets.emojione.path = 'https://cdn.jsdelivr.net/emojione/assets/3.0/png/32/';

// some more settings...
jsemoji.supports_css = false;
jsemoji.allow_native = false;
jsemoji.replace_mode = 'unified';
*/

    }
    //emoji gizleme gösterme
    toggleHiddenEmoji(){
      this.setState({
     isHiddenEmoji: !this.state.isHiddenEmoji
   })
    }




    render() {

        const {store} = this.props;

        const {height} = this.state;

        const style = {
            height: height,
        };

        const EmojiCustomNames = {
    foods: 'Yiyecek ve İçecekler',
    nature: 'Doğa',
    objects: 'Nesneler',
    people : 'İnsan',
    activity: 'Aktiviteler',
    places : 'Yerler',
    flags : 'Bayraklar',
    symbols : 'Semboller',

};

        const Child = () => (
    <EmojiPicker onEmojiClick={this.EmojiCallback} customCategoryNames={EmojiCustomNames}/>
    )


        const activeChannel = store.getActiveChannel();
        const messages = store.getMessagesFromChannel(activeChannel); //store.getMessages();
        const channels = store.getChannels();
        const members = store.getMembersFromChannel(activeChannel);

        const scrollBarStyle = {

          backgroundColor : 'rgba(82,218,139,0.4)',

          msFlexDirection:'column',
          //display : 'none'

        }

        return (

            <div style={style} className="app-messenger">
                <div className="header">
                    <div className="left">

                        <h2>Chat10</h2>
                    </div>
                    <div className="content">

                        {_.get(activeChannel, 'isNew') ? <div className="toolbar">
                            <label>Kime:</label>
                            {
                                members.map((user, key) => {

                                    return <span onClick={() => {

                                        store.removeMemberFromChannel(activeChannel, user);

                                    }} key={key}>{_.get(user, 'name')}</span>
                                })
                            }
                            <input placeholder="İsme Göre Arama Yap" onChange={(event) => {

                                const searchUserText = _.get(event, 'target.value');

                                //console.log("searching for user with name: ", searchUserText)

                                this.setState({
                                    searchUser: searchUserText,
                                    showSearchUser: true,
                                }, () => {


                                    store.startSearchUsers(searchUserText);
                                });


                            }} type="text" value={this.state.searchUser}/>

                            {this.state.showSearchUser ? <SearchUser
                                onSelect={(user) => {

                                    this.setState({
                                        showSearchUser: false,
                                        searchUser: '',

                                    }, () => {


                                        const userId = _.get(user, '_id');
                                        const channelId = _.get(activeChannel, '_id');

                                        store.addUserToChannel(channelId, userId);

                                    });


                                }}
                                store={store}/> : null}

                        </div> : this.renderChannelTitle(activeChannel)}


                    </div>
                    <div className="right">

                        <UserBar store={store}/>

                    </div>
                </div>
                <div className="main">

                    <div className="sidebar-left">

                  <div className = "header">
                            <div className="left">


                          {this.isLogin() ?

                                <button onClick={this._onCreateChannel} className="right-action"><i
                                    className="icon-edit-modify-streamline"/></button>
: null}
                            </div>

                    </div>
                    <div className="sidebar-right">

                                          {members.size > 0 ? <div><h2 className="title">Kişiler</h2>
                                              <div className="members">

                                                  {members.map((member, key) => {


                                                      const isOnline = _.get(member, 'online', false);

                                                      return (
                                                          <div key={key} className="member">
                                                              <div className="user-image">
                                                                  <img src={_.get(member, 'avatar')} alt=""/>
                                                                  <span className={classNames('user-status', {'online': isOnline})} />
                                                              </div>
                                                              <div className="member-info">
                                                                  <h2>{member.name} - <span className={classNames('user-status', {'online': isOnline})}>{isOnline ? 'Çevrimiçi': 'Çevrimdışı'}</span> </h2>
                                                                  <p>Son Giriş: {moment(member.created).fromNow()}</p>
                                                              </div>

                                                          </div>
                                                      )

                                                  })}

                                              </div>
                                          </div> : null}

{this.isLogin() ? <h2 className="title">Odalar</h2> : null}
                                      </div>









                        <div className="chanels">

                            {channels.map((channel, key) => {

                                return (
                                    <div onClick={(key) => {

                                        store.setActiveChannelId(channel._id);

                                    }} key={channel._id}
                                         className={classNames('chanel', {'notify': _.get(channel, 'notify') === true},{'active': _.get(activeChannel, '_id') === _.get(channel, '_id', null)})}>
                                        <div className="user-image">
                                           {this.renderChannelAvatars(channel)}
                                        </div>
                                        <div className="chanel-info">
                                            {this.renderChannelTitle(channel)}
                                            <p>{channel.lastMessage}</p>
                                        </div>
                                        <div>
                                        <div className="icon-settings-streamline-1">
                                        </div>
                                        </div>

                                    </div>
                                )

                            })}


                        </div>
                    </div>
                    <div className="content">

                        <Scrollbars
                        style={scrollBarStyle}
                      //   autoHide={true}
                        //  onScrollStart ={this.scrollMessagesToBottom}
                        //  ref={c => { this.scrollComponent = c }}
                        //  ref="scrollbars"

                         ref={(ref) => this.messagesRef = ref}


                         ref={(ref) => this.messagesRef = ref} className="messages">

                            {messages.map((message, index) => {



                                const user = _.get(message, 'user');


                                return (
                                    <div key={index} className={classNames('message', {'me': message.me})}>

                                        <div className="message-body">

                                        <MessageBox       //{message.me ? 'Sen ' : _.get(message, 'user.name')}

                                    position = {message.me ? 'right' : 'left'}
                                    type={'text'}
                                    text={this.renderMessage(message)}
                                    dateString={moment(message.created).fromNow()}
                                    avatar = {_.get(user, 'avatar')}
                                    title = {message.me ? 'Sen ' : _.get(message, 'user.name')}
                                    titleColor="#2ecc71"

                                />
                                    </div>

                                      </div>
                                )


                            })}


   {!this.state.isHiddenEmoji && <Child />}
                        </Scrollbars  >


                        {activeChannel && members.size > 0 ? <div className="messenger-input">
  <button className="sendEmoji"   onClick={this.toggleHiddenEmoji.bind(this)} ><FaEmojiIcon /></button>


                            <div className="text-input">

										<textarea onKeyUp={(event) => {

                                            if (event.key === 'Enter' && !event.shiftKey) {
                                                this.handleSend();
                                            }


                                        }} onChange={(event) => {


                                            this.setState({newMessage: _.get(event, 'target.value')});

                                        }} value={this.state.newMessage} placeholder="Merhaba..."/>
                            </div>

                            <div  className="actions">

                            </div>

                            <div className="actions">
                                <button onClick={this.handleSend} className="send">Gönder <FaSendIcon/></button>
                            </div>
                        </div> : null}


                    </div>

                </div>
            </div>

        )
    }
}
