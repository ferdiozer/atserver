export const production = true; // set it to true when deploy to the server

const domain = production ? '185.81.153.37:3001' : '185.81.153.37:3001'; // if you have domain pointed to digitalOcean Cloud server let use your domain.eg: tabvn.com
export const websocketUrl = `ws://${domain}`
export const apiUrl = `http://${domain}`